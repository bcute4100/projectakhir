package com.example.travelapp;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

public class FavTourActivity extends AppCompatActivity {
    private DatabaseReference reference;
    ArrayList<ModelRV> list = new ArrayList<>();
    ArrayList<FavModel> favList = new ArrayList<>();
    AdapterRV adapter;
    private RecyclerView mRecycler;
    private LinearLayoutManager mManager;
    private String uid;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fav_tour);

        mRecycler = findViewById(R.id.recyclerView);
        mRecycler.setHasFixedSize(true);

        mManager = new LinearLayoutManager(this);
        mManager.setReverseLayout(true);
        mManager.setStackFromEnd(true);
        mRecycler.setLayoutManager(mManager);

        reference = FirebaseDatabase.getInstance().getReference();
        adapter = new AdapterRV(getApplicationContext(), list);
        mRecycler.setAdapter(adapter);

        uid = FirebaseAuth.getInstance().getUid();
        DatabaseReference favRef = reference.child("favorite").child(uid);

        favRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                favList.clear();

                for (DataSnapshot data : snapshot.getChildren()) {
                    FavModel favModel = data.getValue(FavModel.class);
                    favModel.setId(data.getKey());
                    favList.add(favModel);
                }

                RefreshData();
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {
                Toast.makeText(getApplicationContext(), "Terjadi kesalahan", Toast.LENGTH_SHORT).show();
            }
        });

    }

    public void RefreshData() {
        list.clear();
        for (FavModel fav : favList) {
            DatabaseReference ref = reference.child("tour").child(fav.getTourid());
            ref.addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot snapshot) {
                    ModelRV modelRV = snapshot.getValue(ModelRV.class);
                    modelRV.setFav(fav);
                    modelRV.setId(snapshot.getKey());
                    list.add(modelRV);
                    adapter.notifyDataSetChanged();
                }

                @Override
                public void onCancelled(@NonNull DatabaseError error) {
                    Toast.makeText(getApplicationContext(), "Terjadi kesalahan", Toast.LENGTH_SHORT).show();
                }
            });
        }
    }
}