package com.example.travelapp;

import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;


import com.bumptech.glide.Glide;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import java.util.ArrayList;

public class AdapterRV extends RecyclerView.Adapter<AdapterRV.MyViewHolder>{
    private android.content.Context context;
    private ArrayList<ModelRV> dataTour;


    public AdapterRV(Context cont, ArrayList<ModelRV> data) {
        context = cont;
        dataTour = data;
    }


    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.desain_dashboard_adapter, parent,false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        ModelRV currentDataTour = dataTour.get(position);
        holder.vnama.setText(currentDataTour.getNama());

        Drawable icon;
        if (currentDataTour.getFav() == null) {
            icon = context.getResources().getDrawable(R.drawable.ic_baseline_favorite_border_24);
        } else {
            icon = context.getResources().getDrawable(R.drawable.ic_baseline_favorite_24);
        }
        holder.favButton.setCompoundDrawablesWithIntrinsicBounds(icon, null, null, null);

        Glide.with(context).load(currentDataTour.getImg()).into(holder.vimg);
        holder.vcard.setOnClickListener(view -> {
            Intent intent = new Intent(context,TourDetail.class);
            intent.putExtra("id", currentDataTour.getId());
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            context.startActivity(intent);
        });
        holder.favButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String uid = FirebaseAuth.getInstance().getUid();
                if (currentDataTour.getFav() == null) {
                    FavModel newFav = new FavModel();
                    newFav.setUid(uid);
                    newFav.setTourid(currentDataTour.getId());
                    FirebaseDatabase.getInstance().getReference().child("favorite").child(uid).push().setValue(newFav);
                } else {
                    FirebaseDatabase.getInstance().getReference().child("favorite").child(uid).child(currentDataTour.getFav().getId()).removeValue();
                    currentDataTour.setFav(null);
                }
                notifyDataSetChanged();
            }
        });
    }

    @Override
    public int getItemCount() {
        return dataTour.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView vnama;
        ImageView vimg;
        CardView vcard;
        Button favButton;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);

            vnama = itemView.findViewById(R.id.name_tour);
            vimg = itemView.findViewById(R.id.img_tour);
            vcard = itemView.findViewById(R.id.cv_adapter);
            favButton = itemView.findViewById(R.id.FavButton);
        }
    }
}
