package com.example.travelapp;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.view.MenuProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;


public class LandingPage extends AppCompatActivity {
    private DatabaseReference reference;
    ArrayList<ModelRV> list = new ArrayList<>();
    AdapterRV adapter;
    ModelRV modelRV;
    private RecyclerView mRecycler;
    private LinearLayoutManager mManager;
    private DataSnapshot lastSnapshot;
    private Long pressedTime;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_landing_page);

        mRecycler = findViewById(R.id.recyclerView);
        mRecycler.setHasFixedSize(true);

        mManager = new LinearLayoutManager(this);
        mManager.setReverseLayout(true);
        mManager.setStackFromEnd(true);
        mRecycler.setLayoutManager(mManager);

        reference = FirebaseDatabase.getInstance().getReference();

        DatabaseReference tourRef = reference.child("tour");

        adapter = new AdapterRV(getApplicationContext(), list);
        mRecycler.setAdapter(adapter);
        tourRef.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                OnDataChange(snapshot);
                lastSnapshot = snapshot;
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {
                Toast.makeText(getApplicationContext(), "Terjadi kesalahan", Toast.LENGTH_SHORT).show();
            }
        });

        Toolbar toolbar = (Toolbar)findViewById(R.id.main_toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("TravelKuy");
        toolbar.addMenuProvider(new MenuProvider() {
            @Override
            public void onCreateMenu(@NonNull Menu menu, @NonNull MenuInflater menuInflater) {
                getMenuInflater().inflate(R.menu.main_menu, menu);
            }

            @Override
            public boolean onMenuItemSelected(@NonNull MenuItem menuItem) {
                switch (menuItem.getItemId()){
                    case R.id.action_editprofile:
                        profile();
                        return true;
                    case R.id.action_favtour:
                        favtour();
                        return true;
                    case R.id.action_logout:
                        logOut();
                        return true;
                }
                return false;
            }
        });
    }


    private void OnDataChange(DataSnapshot snapshot) {
        list.clear();
        for(DataSnapshot datasnapshot1: snapshot.getChildren()){
            ModelRV tour = datasnapshot1.getValue(ModelRV.class);
            tour.setId(datasnapshot1.getKey());

            String uid = FirebaseAuth.getInstance().getUid();

            Query query = reference.child("favorite").child(uid).orderByChild("tourid").equalTo(tour.getId());

            query.addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot snapshot) {
                    tour.setFav(null);
                    for (DataSnapshot data : snapshot.getChildren()) {
                        FavModel fav = data.getValue(FavModel.class);
                        fav.setId(data.getKey());
                        tour.setFav(fav);
                        adapter.notifyDataSetChanged();
                    }
                }

                @Override
                public void onCancelled(@NonNull DatabaseError error) {
                    Toast.makeText(getApplicationContext(), "Terjadi kesalahan", Toast.LENGTH_SHORT).show();
                }
            });

            list.add(tour);
            adapter.notifyDataSetChanged();

        }

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 99) {
            OnDataChange(lastSnapshot);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu){
        getMenuInflater().inflate(R.menu.main_menu, menu);
        return true;
    }
    public void logOut(){
        FirebaseAuth mAuth = FirebaseAuth.getInstance();
        mAuth.signOut();
        Intent intent = new Intent(LandingPage.this, LoginPage.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);//makesure user cant go back
        startActivity(intent);
    }

    public void favtour() {
        Intent intent = new Intent (LandingPage.this, FavTourActivity.class);
        startActivityForResult(intent, 99);
    }

    public void profile(){
        Intent intent = new Intent (LandingPage.this, Profile.class);
        startActivity(intent);
    }

//    @Override
//    public void onBackPressed() {
//        if (pressedTime + 2000 > System.currentTimeMillis()){
//            super.onBackPressed();
//            return;
//        }else{
//            Toast.makeText(getBaseContext(), "Press back again to exit", Toast.LENGTH_SHORT).show();
//        }
//        pressedTime = System.currentTimeMillis();
//
//    }
}